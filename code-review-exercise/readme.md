# Code Review Exercise

This directory contains an example of code that isn't used by the Integration Platform Team, but performs a common integration function involving a source system, two target systems, and some logic to filter and transform the data in between.

The code does not function because it makes calls to APIs that don't exist.
The goal of the code is to get a list of users and filter them by a parameter inputted by the user.
Then, the code updates the appropriate system based on the user input.

## Instructions

Read through the code and provide feedback on where it could be improved or where bugs might exist.
Feedback can be documented as comments in the code or by referencing line numbers in a separate document/message.

The code contains no syntax errors.