import requests
import os
import argparse

USER_STORE_API_KEY = os.environ.get('USER_STORE_API_KEY')
EMPLOYEE_API_KEY = os.environ.get('EMPLOYEE_API_KEY')
STUDENT_API_KEY = os.environ.get('STUDENT_API_KEY')

def get_users():
    url = 'https://api.example.wisc.edu/users'
    response = requests.get(url, headers={'Authorization': 'Bearer {}'.format(USER_STORE_API_KEY)})

    return response.json()    
    
def update_students(students):
    for student in students:
        url = "https://api.vendor.com/students"

        student_api_object = {
                'first-name': student['first_name'],
                'last-name': student['first_name'],
                'academic-major': student['major']
        }
 


        response = requests.put('{}/{}'.format(url, student['id']), json=student_api_object, headers={'Authorization': 'Bearer {}'.format(USER_STORE_API_KEY)})
        if response.status_code != 200:
            print('Failed to update student.')


def update_employees(employees):
    url = 'https://api.hrsystem.com/employees'

    for employee in employees:
        employee_api_object = {
                'firstName': employee['first_name'],
                'lastName': employee['last_name'],
                'hiredDate': employee['hired_date']
        }

        if employee['email'] is not None:
            if employee['termination_date'] is None:
                response = requests.put('{}?employee_id={}'.format(url, employee['id']), json=employee_api_object, headers={'Authorization': 'Bearer {}'.format(EMPLOYEE_API_KEY)})

                if response.status_code != 200:
                    print('Failed to update employee ID {}'.format(employee['first_name']))

def filter_by_user_type(users, user_type): 
    filteredUsers = []

    for user in users:
        if user_type in user['roles']:
            filteredUsers.append(user)

    return filteredUsers


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("user_type")
    args = parser.parse_args()

    users = get_users()

    users = filter_by_user_type(users, args.user_type)

    if args.user_type == 'student':
        update_students(users)
    elif args.user_type != 'student' and args.user_type == 'employee':
        update_employees(users)





